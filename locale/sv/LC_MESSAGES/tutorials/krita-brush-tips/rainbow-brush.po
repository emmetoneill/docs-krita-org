# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:51+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/brush-tips/Brushtip-Rainbow.png\n"
"   :alt: selecting fill circle for brush tip"
msgstr ""
".. image:: images/brush-tips/Brushtip-Rainbow.png\n"
"   :alt: Välja fyllcirkel för penselspets"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/brush-tips/Brushtip-Rainbow_2.png\n"
"   :alt: toggle hue in the brush parameter"
msgstr ""
".. image:: images/brush-tips/Brushtip-Rainbow_2.png\n"
"   :alt: Ändra färgton i penselparametrarna"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/brush-tips/Brushtip-Rainbow_3.png\n"
"   :alt: select distance parameter for the hue"
msgstr ""
".. image:: images/brush-tips/Brushtip-Rainbow_3.png\n"
"   :alt: Välja avståndsparameter för färgtonen"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:1
msgid "A tutorial about making rainbow brush in krita"
msgstr "En handledning om hur man skapar en regnbågspensel i Krita"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:13
msgid "Brush-tips:Rainbow Brush"
msgstr "Penselspetsar: Regnbågspensel"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:16
msgid "Question"
msgstr "Fråga"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:18
msgid "**Hello, there is a way to paint with rainbow on Krita?**"
msgstr "**Hej, finns det ett sätt att måla med en regnbåge i Krita?**"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:20
msgid "Yes there is."
msgstr "Ja, det finns det."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:22
msgid "First, select the fill_circle:"
msgstr "Välj först fyllcirkel:"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:27
msgid ""
"Then, press the :kbd:`F5` key to open the brush editor, and toggle **Hue**."
msgstr ""
"Tryck därefter på tangenten :kbd:`F5` för att öppna penseleditorn och ändra "
"**Hå**."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:32
msgid "This should allow you to change the color depending on the pressure."
msgstr "Det ska låta dig ändra färg beroende på tryck."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:34
msgid ""
"The brightness of the rainbow is relative to the color of the currently "
"selected color, so make sure to select bright saturated colors for a bright "
"rainbow!"
msgstr ""
"Regnbågens ljusstyrka är relativ till färgen som för närvarande är vald, så "
"var noga med att välja ljusa mättade färger för en ljusstark regnbåge."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:36
msgid ""
"Uncheck **Pressure** and check **Distance** to make the rainbow paint itself "
"over distance. The slider below can be |mouseright| to change the value with "
"keyboard input."
msgstr ""
"Avmarkera **Tryck** och markera **Avstånd** för att låta regnbågen måla sig "
"själv på avstånd. Skjutreglaget nedan kan högerklickas för att ändra värdet "
"via tangentbordsinmatning."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:41
msgid "When you are satisfied, give the brush a new name and save it."
msgstr "När du är nöjd, ge penseln ett nytt namn och spara den."
