msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___instant_preview.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: 鼠标左键"

#: ../../reference_manual/instant_preview.rst:1
msgid "How the Instant Preview technology in Krita works."
msgstr "介绍 Krita 的即时预览技术的工作原理。"

#: ../../reference_manual/instant_preview.rst:10
#: ../../reference_manual/instant_preview.rst:15
msgid "Instant Preview"
msgstr "即时预览功能"

#: ../../reference_manual/instant_preview.rst:10
msgid "Performance"
msgstr ""

#: ../../reference_manual/instant_preview.rst:10
msgid "Lag"
msgstr ""

#: ../../reference_manual/instant_preview.rst:17
msgid ""
"Instant Preview (previously known under the code name Level Of Detail/LOD "
"strokes) is Krita's special speed-up mechanism that was funded by the 2015 "
"Kickstarter. Krita slows down with really large images due to the large "
"amount of data it's crunching in painting these images. Instant Preview "
"works by taking a smaller version of the canvas, and drawing the feedback on "
"there while Krita calculates the real stroke in the background. This means "
"that if you have a 4k screen and are working on a 4k image at 100% zoom, you "
"won't feel any speed up."
msgstr ""
"即时预览功能是 Krita 针对大型画面和大型笔刷的特殊加速功能。它的工作原理是先直"
"接采样视图显示的画面，画出大致的预览效果，而与此同时在后台对实际的全尺寸图像"
"进行处理，在处理完成后替换掉之前的预览效果。换句话说，如果视图正在按实际大小 "
"(100% 缩放) 显示，这个功能不会带来任何加速效果。"

#: ../../reference_manual/instant_preview.rst:20
msgid "Activating Instant Preview"
msgstr "启用即时预览功能"

#: ../../reference_manual/instant_preview.rst:24
msgid ""
"Instant Preview requires OpenGL 3.0 support at minimum. So if you don't "
"have :guilabel:`high-quality` scaling available in :menuselection:`Settings "
"--> Configure Krita --> Display --> Display scaling filter`, then you won't "
"be able to use Instant Preview either."
msgstr ""
"即时预览功能需要显卡支持 OpenGL 3.0 以上。如果在菜单栏的 :menuselection:`设"
"置 --> 配置 Krita --> 显示 --> 视图缩放算法` 选单中没有 :guilabel:`高质量` 选"
"项，你将无法使用此功能。"

#: ../../reference_manual/instant_preview.rst:29
msgid ".. image:: images/brushes/Lod_position.png"
msgstr ""

#: ../../reference_manual/instant_preview.rst:29
msgid "The Global Instant Preview toggle is under the view menu."
msgstr ""

#: ../../reference_manual/instant_preview.rst:31
msgid ""
"Instant Preview is activated in two places: The view menu (:kbd:`Shift + L` "
"shortcut), and the settings of the given paintop by default. This is because "
"Instant Preview has different limitations with different paint operations."
msgstr ""

#: ../../reference_manual/instant_preview.rst:33
msgid ""
"For example, the overlay mode in the color smudge brush will disable the "
"ability to have Instant Preview on the brush, so does using 'fade' sensor "
"for size."
msgstr ""
"例如，颜色涂抹笔刷引擎使用覆盖模式时、大小选项映射到淡化传感器时都会禁用笔刷"
"的即时预览功能。"

#: ../../reference_manual/instant_preview.rst:35
msgid ""
"Similarly, the auto-spacing, fuzzy sensor in size, use of density in brush-"
"tip and the use of texture paintops will make it more difficult to determine "
"a stroke, and thus will give a feeling of 'popping' when the stroke is "
"finished."
msgstr ""
"有时即使该功能不被强制禁用，也会造成一些问题。例如使用笔尖自动间距时、大小映"
"射到随机度传感器时、笔尖调整密度选项时、在笔刷引擎中使用纹理时，即时预览的效"
"果均不理想，笔画渲染结束并切换的瞬间会明显察觉到预览效果和最终结果存在较大差"
"别。"

#: ../../reference_manual/instant_preview.rst:37
msgid ""
"When you check the brush settings, the Instant Preview checkbox will have a "
"\\* behind it. Hovering over it will give you a list of options that are "
"affecting the Instant Preview mode."
msgstr ""
"在笔刷选项面板 (F5) 中，在“即时预览”字样上点击鼠标左键和右键可以发现一些隐藏"
"的相关提示和功能。"

#: ../../reference_manual/instant_preview.rst:41
msgid ""
"|mouseleft| this pop-up will give a slider, which can be used to determine "
"the threshold size at which instant preview activates. By default this "
"100px. This is useful for brushes that are optimised to work on small sizes."
msgstr ""
"|mouseleft| 左键单击或者悬浮在“即时预览”字样上方可以查看该功能是否可用，在不"
"可用时还会提示当前笔刷大小和触发功能的阈值。右键单击则可以调整该阈值，默认为 "
"100 px。大小低于此阈值的笔刷将无法启用即时预览功能。此功能对于那些要在特定大"
"小下面工作的笔刷很有用。"

#: ../../reference_manual/instant_preview.rst:47
msgid ".. image:: images/brushes/Lod_position2.png"
msgstr ""

#: ../../reference_manual/instant_preview.rst:47
msgid ""
"The Instant Preview checkbox at the bottom of the brush settings editor will "
"give you feedback when there's settings active that can't be previewed "
"right. Hover over it to get more detail. In this case, the issue is that "
"auto-spacing is on."
msgstr ""
"笔刷即时预览功能的开关位置。你可以在它的文字上悬停或者左键单击来查看相关信"
"息，右键单击可以调整阈值。"

#: ../../reference_manual/instant_preview.rst:50
msgid "Tools that benefit from Instant Preview"
msgstr "即时预览功能生效工具"

#: ../../reference_manual/instant_preview.rst:52
msgid "The following tools benefit from Instant Preview:"
msgstr "即使预览功能在下列工具中生效："

#: ../../reference_manual/instant_preview.rst:54
msgid "The Freehand brush tool."
msgstr "手绘笔刷工具"

#: ../../reference_manual/instant_preview.rst:55
msgid "The geometric tools."
msgstr "几何图形工具"

#: ../../reference_manual/instant_preview.rst:56
msgid "The Move Tool."
msgstr "移动工具"

#: ../../reference_manual/instant_preview.rst:57
msgid "The Filters."
msgstr "滤镜"

#: ../../reference_manual/instant_preview.rst:58
msgid "Animation."
msgstr "动画"
