msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../general_concepts/file_formats/file_svg.rst:1
msgid "The Scalable Vector Graphics file format in Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "SVG"
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "*.svg"
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "Scalable Vector Graphics Format"
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:15
msgid "\\*.svg"
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:17
msgid ""
"``.svg``, or Scalable Vector Graphics, is the most modern vector graphics "
"interchange file format out there."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:19
msgid ""
"Being vector graphics, SVG is very light weight. This is because it usually "
"only stores coordinates and parameters for the maths involved with vector "
"graphics."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:21
msgid ""
"It is maintained by the W3C SVG working group, who also maintain other open "
"standards that make up our modern internet."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:23
msgid ""
"While you can open up SVG files with any text-editor to edit them, it is "
"best to use a vector program like Inkscape. Krita 2.9 to 3.3 supports "
"importing SVG via the add shape docker. Since Krita 4.0, SVGs can be "
"properly imported, and you can export singlevector layers via :menuselection:"
"`Layer --> Import/Export --> Save Vector Layer as SVG...`. For 4.0, Krita "
"will also use SVG to save vector data into its :ref:`internal format "
"<file_kra>`."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:25
msgid ""
"SVG is designed for the internet, though sadly, because vector graphics are "
"considered a bit obscure compared to raster graphics, not a lot of websites "
"accept them yet. Hosting them on your own webhost works just fine though."
msgstr ""
