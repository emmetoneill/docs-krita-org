# Translation of docs_krita_org_reference_manual___preferences___color_management_settings.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-17 18:21+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/preferences/color_management_settings.rst:1
msgid "The color management settings in Krita."
msgstr "Ajustaments per a la gestió del color al Krita."

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Preferences"
msgstr "Preferències"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Settings"
msgstr "Ajustaments"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color Management"
msgstr "Gestió del color"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color"
msgstr "Color"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Softproofing"
msgstr "Provatura suau"

#: ../../reference_manual/preferences/color_management_settings.rst:17
msgid "Color Management Settings"
msgstr "Ajustaments per a la gestió del color"

#: ../../reference_manual/preferences/color_management_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Color_Management.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Color_Management.png"

#: ../../reference_manual/preferences/color_management_settings.rst:21
msgid ""
"Krita offers extensive functionality for color management, utilising `Little "
"CMS <http://www.littlecms.com/>`_ We describe Color Management in a more "
"overall level here: :ref:`color_managed_workflow`."
msgstr ""
"El Krita ofereix una àmplia funcionalitat per a la gestió del color, "
"utilitzant `Little CMS <https://www.littlecms.com/>`_. Aquí descrivim la "
"Gestió del color en un nivell més general: :ref:`color_managed_workflow`."

#: ../../reference_manual/preferences/color_management_settings.rst:25
msgid "General"
msgstr "General"

#: ../../reference_manual/preferences/color_management_settings.rst:28
msgid "Default Color Model For New Images"
msgstr "Model de color predeterminat per a les imatges noves"

#: ../../reference_manual/preferences/color_management_settings.rst:30
msgid "Choose the default model you prefer for all your images."
msgstr ""
"Trieu el model predeterminat que preferiu per a totes les vostres imatges."

#: ../../reference_manual/preferences/color_management_settings.rst:33
msgid "When Pasting Into Krita From Other Applications"
msgstr "Quan s'enganxa al Krita des d'altres aplicacions"

#: ../../reference_manual/preferences/color_management_settings.rst:35
msgid ""
"The user can define what kind of conversion, if any, Krita will do to an "
"image that is copied from other applications i.e. Browser, GIMP, etc."
msgstr ""
"L'usuari podrà definir quin tipus de conversió, si escau, farà el Krita a "
"una imatge que s'ha copiat des d'una altra aplicació, és a dir, Navegador, "
"GIMP, etc."

#: ../../reference_manual/preferences/color_management_settings.rst:37
msgid "Assume sRGB"
msgstr "Assumeix sRGB"

#: ../../reference_manual/preferences/color_management_settings.rst:38
msgid ""
"This option will show the pasted image in the default Krita ICC profile of "
"sRGB."
msgstr ""
"Aquesta opció mostrarà la imatge enganxada en el perfil ICC predeterminat "
"del Krita, el qual és sRGB."

#: ../../reference_manual/preferences/color_management_settings.rst:39
msgid "Assume monitor profile"
msgstr "Assumeix el perfil del monitor"

#: ../../reference_manual/preferences/color_management_settings.rst:40
msgid ""
"This option will show the pasted image in the monitor profile selected in "
"system preferences."
msgstr ""
"Aquesta opció mostrarà la imatge enganxada en el perfil del monitor "
"seleccionat a les preferències del sistema."

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid "Ask each time"
msgstr "Pregunta cada cop"

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid ""
"Krita will ask the user each time an image is pasted, what to do with it. "
"This is the default."
msgstr ""
"El Krita preguntarà a l'usuari cada vegada que s'enganxi una imatge. Què "
"fer? Aquest és el valor predeterminat."

#: ../../reference_manual/preferences/color_management_settings.rst:46
msgid ""
"When copying and pasting in Krita color information is always preserved."
msgstr ""
"Quan copieu i enganxeu en el Krita, sempre es mantindrà la informació del "
"color."

#: ../../reference_manual/preferences/color_management_settings.rst:49
msgid "Use Blackpoint Compensation"
msgstr "Usa la compensació del punt negre"

#: ../../reference_manual/preferences/color_management_settings.rst:51
msgid ""
"This option will turn on Blackpoint Compensation for the conversion. BPC is "
"explained by the maintainer of LCMS as following:"
msgstr ""
"Aquesta opció activarà la Compensació del punt negre per a la conversió. La "
"BPC («Blackpoint Compensation») l'explica el mantenidor de LCMS de la "
"següent manera:"

#: ../../reference_manual/preferences/color_management_settings.rst:53
msgid ""
"BPC is a sort of \"poor man's\" gamut mapping. It basically adjust contrast "
"of images in a way that darkest tone of source device gets mapped to darkest "
"tone of destination device. If you have an image that is adjusted to be "
"displayed on a monitor, and want to print it on a large format printer, you "
"should realize printer can render black significantly darker that the "
"screen. So BPC can do the adjustment for you. It only makes sense on "
"Relative colorimetric intent. Perceptual and Saturation does have an "
"implicit BPC."
msgstr ""
"La BPC és una espècie de mapeig de la gamma per a «pobres». Bàsicament, "
"ajusta el contrast de les imatges de manera que el to més fosc del "
"dispositiu d'origen s'assigna al to més fosc del dispositiu de destinació. "
"Si teniu una imatge que està ajustada per mostrar-se en un monitor i voleu "
"imprimir-la en una impressora de gran format, haureu d'adonar-vos que la "
"impressora podria ennegrir significativament més que la pantalla. De manera "
"que la BPC ho podrà ajustar. Només té sentit en el propòsit de la "
"colorimetria relativa. La Percepció i la Saturació tenen una BPC implícita."

#: ../../reference_manual/preferences/color_management_settings.rst:56
msgid "Allow LittleCMS optimizations"
msgstr "Permetre les optimitzacions de LittleCMS"

#: ../../reference_manual/preferences/color_management_settings.rst:58
msgid "Uncheck this option when using Linear Light RGB or XYZ."
msgstr "Desmarqueu aquesta opció quan utilitzeu llum lineal RGB o XYZ."

#: ../../reference_manual/preferences/color_management_settings.rst:61
msgid "Display"
msgstr "Pantalla"

#: ../../reference_manual/preferences/color_management_settings.rst:63
msgid "Use System Monitor Profile"
msgstr "Usa el perfil del monitor del sistema"

#: ../../reference_manual/preferences/color_management_settings.rst:64
msgid ""
"This option when selected will tell Krita to use the ICC profile selected in "
"your system preferences."
msgstr ""
"Quan se seleccioni aquesta opció s'indicarà al Krita que utilitzi el perfil "
"ICC seleccionat a les preferències del vostre sistema."

#: ../../reference_manual/preferences/color_management_settings.rst:65
msgid "Screen Profiles"
msgstr "Perfils de la pantalla"

#: ../../reference_manual/preferences/color_management_settings.rst:66
msgid ""
"There are as many of these as you have screens connected. The user can "
"select an ICC profile which Krita will use independent of the monitor "
"profile set in system preferences. The default is sRGB built-in. On Unix "
"systems, profile stored in $/usr/share/color/icc (system location) or $~/."
"local/share/color/icc (local location) will be proposed. Profile stored in "
"Krita preference folder, $~/.local/share/krita/profiles will be visible only "
"in Krita."
msgstr ""
"N'hi haurà tants com pantalles connectades. L'usuari podrà seleccionar un "
"perfil ICC que el Krita utilitzarà independentment del perfil del monitor "
"establert a les preferències del sistema. El valor predeterminat és sRGB "
"incorporat. En els sistemes Unix, es proposarà el perfil emmagatzemat a $/"
"usr/share/color/icc (ubicació del sistema) o $~/.local/share/color/icc "
"(ubicació local). El perfil emmagatzemat a la carpeta de preferències del "
"Krita, $~/.local/share/krita/profiles només serà visible en el Krita."

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid "Rendering Intent"
msgstr "Propòsit de la representació"

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid ""
"Your choice of rendering intents is a way of telling Littlecms how you want "
"colors mapped from one color space to another. There are four options "
"available, all are explained on the :ref:`icc_profiles` manual page."
msgstr ""
"L'elecció dels propòsits de la representació és una forma de dir-li a "
"Littlecms com voleu que els colors s'assignin d'un espai de color a un "
"altre. Hi ha quatre opcions disponibles, totes s'expliquen en la pàgina de "
"manual :ref:`icc_profiles`."

#: ../../reference_manual/preferences/color_management_settings.rst:71
msgid "Softproofing options"
msgstr "Opcions per a la provatura suau"

#: ../../reference_manual/preferences/color_management_settings.rst:73
msgid ""
"These allow you to configure the *default* softproofing options. To "
"configure the actual softproofing for the current image, go to :"
"menuselection:`Image --> Image Properties --> Softproofing` ."
msgstr ""
"Permeten configurar les opcions *predeterminades* per a la provatura suau. "
"Per a configurar la provatura suau real per a la imatge actual, aneu a :"
"menuselection:`Imatge --> Propietats de la imatge --> Provatura suau`."

# skip-rule: t-acc_obe
#: ../../reference_manual/preferences/color_management_settings.rst:75
msgid ""
"For indepth details about how to use softproofing, check out :ref:`the page "
"on softproofing <soft_proofing>`."
msgstr ""
"Per a detalls en profunditat sobre com utilitzar la provatura suau, reviseu :"
"ref:`la pàgina sobre la provatura suau <soft_proofing>`."
