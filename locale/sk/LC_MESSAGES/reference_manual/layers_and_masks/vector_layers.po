# translation of docs_krita_org_reference_manual___layers_and_masks___vector_layers.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___vector_layers\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-04-02 10:52+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
#, fuzzy
#| msgid ".. image:: images/en/400px-Fill_rule_even-odd.svg.png"
msgid ".. image:: images/vector/Fill_rule_even-odd.svg"
msgstr ".. image:: images/en/400px-Fill_rule_even-odd.svg.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
#, fuzzy
#| msgid ".. image:: images/en/400px-Fill_rule_non-zero.svg.png"
msgid ".. image:: images/vector/Fill_rule_non-zero.svg"
msgstr ".. image:: images/en/400px-Fill_rule_non-zero.svg.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:1
msgid "How to use vector layers in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
#, fuzzy
#| msgid "Vector Layers"
msgid "Vector"
msgstr "Vektorové vrstvy"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
#, fuzzy
#| msgid "Vector Layers"
msgid "Layers"
msgstr "Vektorové vrstvy"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:20
msgid "Vector Layers"
msgstr "Vektorové vrstvy"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:24
msgid ""
"This page is outdated. Check :ref:`vector_graphics` for a better overview."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:27
msgid "What is a Vector Layer?"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:29
msgid ""
"A Vector Layers, also known as a shape layer, is a type of layers that "
"contains only vector elements."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:31
msgid ""
"This is how vector layers will appear in the :program:`Krita` Layers docker."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:34
#, fuzzy
#| msgid ".. image:: images/en/Vectorlayer.png"
msgid ".. image:: images/vector/Vectorlayer.png"
msgstr ".. image:: images/en/Vectorlayer.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:35
msgid ""
"It shows the vector contents of the layer on the left side. The icon showing "
"the page with the red bookmark denotes that it is a vector layer. To the "
"right of that is the layer name. Next are the layer visibility and "
"accessibility icons. Clicking the \"eye\" will toggle visibility. Clicking "
"the lock into a closed position will lock the content and editing will no "
"longer be allowed until it is clicked again and the lock on the layer is "
"released."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:38
msgid "Creating a vector layer"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:40
msgid ""
"You can create a vector layer in two ways. Using the extra options from the "
"\"Add Layer\" button you can click the \"Vector Layer\" item and it will "
"create a new vector layer. You can also drag a rectangle or ellipse from the "
"**Add shape** dock onto an active Paint Layer.  If the active layer is a "
"Vector Layer then the shape will be added directly to it."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:43
msgid "Editing Shapes on a Vector Layer"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:47
msgid ""
"There's currently a bug with the vector layers that they will always "
"consider themselves to be at 72dpi, regardless of the actual pixel-size. "
"This can make manipulating shapes a little difficult, as the precise input "
"will not allow cm or inch, even though the vector layer coordinate system "
"uses those as a basis."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:50
msgid "Basic Shape Manipulation"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:52
msgid ""
"To edit the shape and colors of your vector element, you will need to use "
"the basic shape manipulation tool."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:54
msgid ""
"Once you have selected this tool, click on the element you want to "
"manipulate and you will see guides appear around your shape."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:57
#, fuzzy
#| msgid ".. image:: images/en/Vectorguides.png"
msgid ".. image:: images/vector/Vectorguides.png"
msgstr ".. image:: images/en/Vectorguides.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:58
msgid ""
"There are four ways to manipulate your image using this tool and the guides "
"on your shape."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:61
msgid "Transform/Move"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:64
#, fuzzy
#| msgid ":.. image:: /images/en/Transform.png"
msgid ".. image:: images/vector/Transform.png"
msgstr ":.. image:: /images/en/Transform.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:65
msgid ""
"This feature of the tool allows you to move your object by clicking and "
"dragging your shape around the canvas. Holding the :kbd:`Ctrl` key will lock "
"your moves to one axis."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:68
msgid "Size/Stretch"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:71
#, fuzzy
#| msgid ":.. image:: /images/en/Resize.png"
msgid ".. image:: images/vector/Resize.png"
msgstr ":.. image:: /images/en/Resize.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:72
msgid ""
"This feature of the tool allows you to stretch your shape.  Selecting a "
"midpoint will allow stretching along one axis. Selecting a corner point will "
"allow stretching across both axis. Holding the :kbd:`Shift` key will allow "
"you to scale your object. Holding the :kbd:`Ctrl` key will cause your "
"manipulation to be mirrored across your object."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:75
msgid "Rotate"
msgstr "Otočiť"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:78
#, fuzzy
#| msgid ":.. image:: /images/en/Rotatevector.png"
msgid ".. image:: images/vector/Rotatevector.png"
msgstr ":.. image:: /images/en/Rotatevector.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:79
msgid ""
"This feature of the tool will allow you to rotate your object around its "
"center. Holding the :kbd:`Ctrl` key will cause your rotation to lock to 45 "
"degree angles."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:82
msgid "Skew"
msgstr "Skosiť"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:85
#, fuzzy
#| msgid ":.. image:: /images/en/Skew.png"
msgid ".. image:: images/vector/Skew.png"
msgstr ":.. image:: /images/en/Skew.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:86
msgid "This feature of the tool will allow you to skew your object."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:90
msgid ""
"At the moment there is no way to scale only one side of your vector object. "
"The developers are aware that this could be useful and will work on it as "
"manpower allows."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:93
msgid "Point and Curve Shape Manipulation"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:95
msgid ""
"Double-click on a vector object to edit the specific points or curves which "
"make up the shape. Click and drag a point to move it around the canvas. "
"Click and drag along a line to curve it between two points. Holding the :kbd:"
"`Ctrl` key will lock your moves to one axis."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:98
#, fuzzy
#| msgid ".. image:: images/en/Pointcurvemanip.png"
msgid ".. image:: images/vector/Pointcurvemanip.png"
msgstr ".. image:: images/en/Pointcurvemanip.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:100
msgid "Stroke and Fill"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:102
msgid ""
"In addition to being defined by points and curves, a shape also has two "
"defining properties: **Fill** and **Stroke**. **Fill** defines the color, "
"gradient, or pattern that fills the space inside of the shape object. "
"'**Stroke**' defines the color, gradient, pattern, and thickness of the "
"border along the edge of the shape. These two can be edited using the "
"**Stroke and Fill** dock. The dock has two modes. One for stroke and one for "
"fill. You can change modes by clicking in the dock on the filled square or "
"the black line. The active mode will be shown by which is on top of the "
"other."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:104
msgid ""
"Here is the dock with the fill element active. Notice the red line across "
"the solid white square. This tells us that there is no fill assigned "
"therefore the inside of the shape will be transparent."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:107
#: ../../reference_manual/layers_and_masks/vector_layers.rst:129
#, fuzzy
#| msgid ".. image:: images/en/Strokeandfill.png"
msgid ".. image:: images/vector/Strokeandfill.png"
msgstr ".. image:: images/en/Strokeandfill.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:108
msgid "Here is the dock with the stroke element active."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:111
#, fuzzy
#| msgid ".. image:: images/en/Strokeandfillstroke.png"
msgid ".. image:: images/vector/Strokeandfillstroke.png"
msgstr ".. image:: images/en/Strokeandfillstroke.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:113
msgid "Editing Stroke Properties"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:115
msgid ""
"The stroke properties dock will allow you to edit a different aspect of how "
"the outline of your vector shape looks."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:118
#, fuzzy
#| msgid ".. image:: images/en/Strokeprops.png"
msgid ".. image:: images/vector/Strokeprops.png"
msgstr ".. image:: images/en/Strokeprops.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:119
msgid ""
"The style selector allows you to choose different patterns and line styles. "
"The width option changes the thickness of the outline on your vector shape. "
"The cap option changes how line endings appear. The join option changes how "
"corners appear."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:121
msgid ""
"The Miter limit controls how harsh the corners of your object will display. "
"The higher the number the more the corners will be allowed to stretch out "
"past the points. Lower numbers will restrict the stroke to shorter and less "
"sharp corners."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:124
msgid "Editing Fill Properties"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:126
msgid ""
"All of the fill properties are contained in the **Stroke and Fill** dock."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:130
msgid ""
"The large red **X** button will set the fill to none causing the area inside "
"of the vector shape to be transparent."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:132
msgid ""
"To the right of that is the solid square. This sets the fill to be a solid "
"color which is displayed in the long button and can be selected by pressing "
"the arrow just to the right of the long button. To the right of the solid "
"square is the gradient button. This will set the fill to display as a "
"gradient. A gradient can be selected by pressing the down arrow next to the "
"long button."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:134
msgid ""
"Under the **X** is a button that shows a pattern. This inside area will be "
"filled with a pattern. A pattern can be chosen by pressing the arrows next "
"to the long button. The two other buttons are for **fill rules**: the way a "
"self-overlapping path is filled."
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:136
msgid ""
"The button with the inner square blank toggles even-odd mode, where every "
"filled region of the path is next to an unfilled one, like this:"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:142
msgid ""
"The button with the inner square filled toggles non zero mode, where most of "
"the time a self overlapping path is entirely filled except when it overlaps "
"with a sub-path of a different direction that 'decrease the level of "
"overlapping' so that the region between the two is considered outside the "
"path and remain unfilled, like this:"
msgstr ""

#: ../../reference_manual/layers_and_masks/vector_layers.rst:148
msgid ""
"For more (and better) information about fill rules check the `Inkscape "
"manual <http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Attributes-Fill-Stroke."
"html#Attributes-Fill-Rule>`_."
msgstr ""
