# translation of docs_krita_org_reference_manual___main_menu___edit_menu.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___main_menu___edit_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-04-02 12:05+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/main_menu/edit_menu.rst:1
msgid "The edit menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:19
msgid "Undo"
msgstr "Späť"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:22
msgid "Redo"
msgstr "Znovu"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:25
msgid "Cut"
msgstr "Vystrihnúť"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:28
msgid "Copy"
msgstr "Kopírovať"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:40
msgid "Paste"
msgstr "Vložiť"

#: ../../reference_manual/main_menu/edit_menu.rst:11
msgid "Edit"
msgstr "Upraviť"

#: ../../reference_manual/main_menu/edit_menu.rst:16
#, fuzzy
#| msgid "Edit"
msgid "Edit Menu"
msgstr "Upraviť"

#: ../../reference_manual/main_menu/edit_menu.rst:21
msgid "Undoes the last action. Shortcut: :kbd:`Ctrl + Z`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:24
msgid "Redoes the last undone action. Shortcut: :kbd:`Ctrl + Shift+ Z`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:27
msgid "Cuts the selection or layer. Shortcut: :kbd:`Ctrl + X`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:30
msgid "Copies the selection or layer. Shortcut: :kbd:`Ctrl + C`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:31
msgid "Cut (Sharp)"
msgstr "Vystrihnúť (ostro)"

#: ../../reference_manual/main_menu/edit_menu.rst:33
msgid ""
"This prevents semi-transparent areas from appearing on your cut pixels, "
"making them either fully opaque or fully transparent."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:34
msgid "Copy (Sharp)"
msgstr "Kopírovať (ostro)"

#: ../../reference_manual/main_menu/edit_menu.rst:36
msgid "Same as :term:`Cut (Sharp)` but then copying instead."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:37
msgid "Copy Merged"
msgstr "Kopírovať zlúčené"

#: ../../reference_manual/main_menu/edit_menu.rst:39
msgid "Copies the selection over all layers. Shortcut: :kbd:`Ctrl + Shift + C`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:42
msgid ""
"Pastes the copied buffer into the image as a new layer. Shortcut: :kbd:`Ctrl "
"+ V`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:43
msgid "Paste at Cursor"
msgstr "Vložiť na pozíciu kurzora"

#: ../../reference_manual/main_menu/edit_menu.rst:45
msgid "Same as :term:`paste`, but aligns the image to the cursor."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:46
msgid "Paste into new image"
msgstr "Vložiť do nového obrázku"

#: ../../reference_manual/main_menu/edit_menu.rst:48
msgid "Pastes the copied buffer into a new image."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:49
msgid "Clear"
msgstr "Vyčistiť"

#: ../../reference_manual/main_menu/edit_menu.rst:51
msgid "Clear the current layer. Shortcut: :kbd:`Del`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:52
#, fuzzy
#| msgid "Fill with pattern"
msgid "Fill with Foreground Color"
msgstr "Vyplniť vzorom"

#: ../../reference_manual/main_menu/edit_menu.rst:54
msgid ""
"Fills the layer or selection with the foreground color. Shortcut: :kbd:"
"`Shift + Backspace`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:55
#, fuzzy
#| msgid "Fill with pattern"
msgid "Fill with Background Color"
msgstr "Vyplniť vzorom"

#: ../../reference_manual/main_menu/edit_menu.rst:57
msgid ""
"Fills the layer or selection with the background color. Shortcut: :kbd:"
"`Backspace`"
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:58
msgid "Fill with pattern"
msgstr "Vyplniť vzorom"

#: ../../reference_manual/main_menu/edit_menu.rst:60
msgid "Fills the layer or selection with the active pattern."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:61
msgid "Stroke Selected Shapes"
msgstr "Ťahať vybrané tvary"

#: ../../reference_manual/main_menu/edit_menu.rst:63
msgid ""
"Strokes the selected vector shape with the selected brush, will create a new "
"layer."
msgstr ""

#: ../../reference_manual/main_menu/edit_menu.rst:64
msgid "Stroke Selection"
msgstr "Výber ťahu"

#: ../../reference_manual/main_menu/edit_menu.rst:66
msgid "Strokes the active selection using the menu."
msgstr ""
