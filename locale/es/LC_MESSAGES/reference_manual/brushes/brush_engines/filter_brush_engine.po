# Spanish translations for docs_krita_org_reference_manual___brushes___brush_engines___filter_brush_engine.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___filter_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-19 22:09+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:1
msgid "The Filter Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:17
msgid "Filter Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Motor de pinceles"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:20
msgid ".. image:: images/icons/filterbrush.svg"
msgstr ".. image:: images/icons/filterbrush.svg"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:21
msgid ""
"Where in other programs you have a 'dodge tool', 'blur tool' and 'sharpen "
"tool', Krita has a special brush engine for this: The Filter Brush engine. "
"On top of that, due to Krita's great integration of the filters, a huge "
"amount of filters you'd never thought you wanted to use for a drawing are "
"possible in brush form too!"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:24
msgid "Options"
msgstr "Opciones"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:26
msgid "The filter brush has of course some basic brush-system parameters:"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:28
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:31
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:32
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"
