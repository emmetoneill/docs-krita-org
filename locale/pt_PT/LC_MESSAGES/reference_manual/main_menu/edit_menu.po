# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 10:23+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: term Krita shift kbd Del Backspace\n"

#: ../../reference_manual/main_menu/edit_menu.rst:1
msgid "The edit menu in Krita."
msgstr "O menu de edição no Krita."

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:19
msgid "Undo"
msgstr "Desfazer"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:22
msgid "Redo"
msgstr "Refazer"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:25
msgid "Cut"
msgstr "Cortar"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:28
msgid "Copy"
msgstr "Copiar"

#: ../../reference_manual/main_menu/edit_menu.rst:11
#: ../../reference_manual/main_menu/edit_menu.rst:40
msgid "Paste"
msgstr "Colar"

#: ../../reference_manual/main_menu/edit_menu.rst:11
msgid "Edit"
msgstr "Editar"

#: ../../reference_manual/main_menu/edit_menu.rst:16
msgid "Edit Menu"
msgstr "O Menu Editar"

#: ../../reference_manual/main_menu/edit_menu.rst:21
msgid "Undoes the last action. Shortcut: :kbd:`Ctrl + Z`"
msgstr "Anula a última acção. Atalho: :kbd:`Ctrl + Z`"

#: ../../reference_manual/main_menu/edit_menu.rst:24
msgid "Redoes the last undone action. Shortcut: :kbd:`Ctrl + Shift+ Z`"
msgstr "Repete a última acção anulada. Atalho: :kbd:`Ctrl + shift+ Z`"

#: ../../reference_manual/main_menu/edit_menu.rst:27
msgid "Cuts the selection or layer. Shortcut: :kbd:`Ctrl + X`"
msgstr "Corta a selecção ou camada. Atalho: :kbd:`Ctrl + X`"

#: ../../reference_manual/main_menu/edit_menu.rst:30
msgid "Copies the selection or layer. Shortcut: :kbd:`Ctrl + C`"
msgstr "Copia a selecção ou camada. Atalho: :kbd:`Ctrl + C`"

#: ../../reference_manual/main_menu/edit_menu.rst:31
msgid "Cut (Sharp)"
msgstr "Cortar (Directamente)"

#: ../../reference_manual/main_menu/edit_menu.rst:33
msgid ""
"This prevents semi-transparent areas from appearing on your cut pixels, "
"making them either fully opaque or fully transparent."
msgstr ""
"Isto impede as áreas semi-transparentes de aparecerem nos seus pixels "
"cortados, tornando-os completamente opacos ou transparentes."

#: ../../reference_manual/main_menu/edit_menu.rst:34
msgid "Copy (Sharp)"
msgstr "Copiar (Directamente)"

#: ../../reference_manual/main_menu/edit_menu.rst:36
msgid "Same as :term:`Cut (Sharp)` but then copying instead."
msgstr "Igual ao :term:`Cortar (Directamente)` mas com cópia em vez de corte."

#: ../../reference_manual/main_menu/edit_menu.rst:37
msgid "Copy Merged"
msgstr "Copiar Reunido"

#: ../../reference_manual/main_menu/edit_menu.rst:39
msgid "Copies the selection over all layers. Shortcut: :kbd:`Ctrl + Shift + C`"
msgstr "Copia a selecção por todas as camadas. Atalho: :kbd:`Ctrl + Shift + C`"

#: ../../reference_manual/main_menu/edit_menu.rst:42
msgid ""
"Pastes the copied buffer into the image as a new layer. Shortcut: :kbd:`Ctrl "
"+ V`"
msgstr ""
"Cola os dados copiados na imagem como uma nova camada. Atalho: :kbd:`Ctrl + "
"V`"

#: ../../reference_manual/main_menu/edit_menu.rst:43
msgid "Paste at Cursor"
msgstr "Colar no Cursor"

#: ../../reference_manual/main_menu/edit_menu.rst:45
msgid "Same as :term:`paste`, but aligns the image to the cursor."
msgstr "Igual ao :term:`colar`, mas alinha a imagem com o cursor."

#: ../../reference_manual/main_menu/edit_menu.rst:46
msgid "Paste into new image"
msgstr "Colar numa nova imagem"

#: ../../reference_manual/main_menu/edit_menu.rst:48
msgid "Pastes the copied buffer into a new image."
msgstr "Cola os dados copiados numa nova imagem."

#: ../../reference_manual/main_menu/edit_menu.rst:49
msgid "Clear"
msgstr "Limpar"

#: ../../reference_manual/main_menu/edit_menu.rst:51
msgid "Clear the current layer. Shortcut: :kbd:`Del`"
msgstr "Limpa a camada actual. Atalho: :kbd:`Del`"

#: ../../reference_manual/main_menu/edit_menu.rst:52
msgid "Fill with Foreground Color"
msgstr "Preencher com a Cor Principal"

#: ../../reference_manual/main_menu/edit_menu.rst:54
msgid ""
"Fills the layer or selection with the foreground color. Shortcut: :kbd:"
"`Shift + Backspace`"
msgstr ""
"Preenche a camada ou selecção com a cor principal. Atalho: :kbd:`Shift + "
"Backspace`"

#: ../../reference_manual/main_menu/edit_menu.rst:55
msgid "Fill with Background Color"
msgstr "Preencher com a Cor de Fundo"

#: ../../reference_manual/main_menu/edit_menu.rst:57
msgid ""
"Fills the layer or selection with the background color. Shortcut: :kbd:"
"`Backspace`"
msgstr ""
"Preenche a camada ou selecção com a cor de fundo. Atalho: :kbd:`Backspace`"

#: ../../reference_manual/main_menu/edit_menu.rst:58
msgid "Fill with pattern"
msgstr "Preencher com o Padrão"

#: ../../reference_manual/main_menu/edit_menu.rst:60
msgid "Fills the layer or selection with the active pattern."
msgstr "Preenche a camada ou selecção com o padrão activo de momento."

#: ../../reference_manual/main_menu/edit_menu.rst:61
msgid "Stroke Selected Shapes"
msgstr "Traçar as Formas Seleccionadas"

#: ../../reference_manual/main_menu/edit_menu.rst:63
msgid ""
"Strokes the selected vector shape with the selected brush, will create a new "
"layer."
msgstr ""
"Pinta a forma vectorial seleccionada com o pincel seleccionado, criando uma "
"nova camada."

#: ../../reference_manual/main_menu/edit_menu.rst:64
msgid "Stroke Selection"
msgstr "Traçar a Selecção"

#: ../../reference_manual/main_menu/edit_menu.rst:66
msgid "Strokes the active selection using the menu."
msgstr "Cria traços sobre a selecção activa, usando o menu."
