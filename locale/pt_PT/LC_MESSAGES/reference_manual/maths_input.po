# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-19 23:22+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: asin cos Co log atan Krita acos exp co tan sin abs\n"

#: ../../reference_manual/maths_input.rst:1
msgid ""
"Overview of maths operations that can be used in Krita spinboxes and number "
"inputs."
msgstr ""
"Introdução às operações matemáticas que podem ser usadas nos campos "
"incrementais e campos numéricos do Krita."

#: ../../reference_manual/maths_input.rst:12
msgid "Maths"
msgstr "Matemática"

#: ../../reference_manual/maths_input.rst:17
msgid "Maths Input"
msgstr "Introdução Matemática"

#: ../../reference_manual/maths_input.rst:19
msgid ""
"Also known as Numerical Input boxes. You can make Krita do simple maths for "
"you in the places where we have number input. Just select the number in a "
"spinbox, or right-click a slider to activate number input. It doesn't do "
"unit conversion yet, but this is planned."
msgstr ""
"Também conhecidas como campos Numéricos. Poderá fazer com que o Krita faça "
"contas matemáticas simples por si nos locais onde temos dados numéricos. "
"Basta seleccionar o número num campo incremental ou carregar com o botão "
"direito sobre um campo incremental para activar a introdução de números. Não "
"faz ainda a conversão de números, mas está planeado fazê-lo."

#: ../../reference_manual/maths_input.rst:22
msgid "Possible Functions"
msgstr "Funções Possíveis"

#: ../../reference_manual/maths_input.rst:25
msgid "Just adds the numbers. Usage: ``50+100`` Output: ``150``"
msgstr ""
"Simplesmente adiciona os números. Utilização: ``50+100`` Resultado: ``150``"

#: ../../reference_manual/maths_input.rst:26
msgid "Addition (Operator: + )"
msgstr "Adição (Operador: + )"

#: ../../reference_manual/maths_input.rst:29
msgid ""
"Just subtracts the last number from the first. Usage: ``50-100`` Output: "
"``50``"
msgstr ""
"Simplesmente subtrai o último número do primeiro. Utilização: ``50-100`` "
"Resultado: ``-50``"

#: ../../reference_manual/maths_input.rst:30
msgid "Subtraction (Operator: - )"
msgstr "Subtracção (Operador: - )"

#: ../../reference_manual/maths_input.rst:33
msgid "Just multiplies the numbers. Usage: ``50*100`` Output: ``5000``"
msgstr ""
"Simplesmente multiplica os valores. Utilização: ``50*100`` Resultado: "
"``5000``"

#: ../../reference_manual/maths_input.rst:34
msgid "Multiplication (Operator: * )"
msgstr "Multiplicação (Operador: * )"

#: ../../reference_manual/maths_input.rst:37
msgid "Just divides the numbers. Usage: ``50/100`` Output: ``0.5``"
msgstr ""
"Simplesmente divide os valores. Utilização: ``50/100`` Resultado: ``0,5``"

#: ../../reference_manual/maths_input.rst:38
msgid "Division (Operator: / )"
msgstr "Divisão (Operador: / )"

#: ../../reference_manual/maths_input.rst:41
msgid ""
"Makes the last number the exponent of the first and calculates the result. "
"Usage: ``2^8`` Output: ``256``"
msgstr ""
"Usa o último número como expoente do primeiro e calcula o resultado. "
"Utilização: ``2^8`` Resultado: ``256``"

#: ../../reference_manual/maths_input.rst:42
msgid "Exponent (Operator: ^ )"
msgstr "Expoente (Operador: ^ )"

#: ../../reference_manual/maths_input.rst:45
msgid ""
"Gives you the sine of the given angle. Usage: ``sin(50)`` Output: ``0.76``"
msgstr ""
"Devolve-lhe o seno do ângulo indicado. Utilização: ``sin(50)`` Resultado: "
"``0,76``"

#: ../../reference_manual/maths_input.rst:46
msgid "Sine (Operator: sin() )"
msgstr "Seno (Operador: sin() )"

#: ../../reference_manual/maths_input.rst:49
msgid ""
"Gives you the cosine of the given angle. Usage: ``cos(50)`` Output: ``0.64``"
msgstr ""
"Devolve-lhe o co-seno do ângulo indicado. Utilização: ``cos(50)`` Resultado: "
"``0,64``"

#: ../../reference_manual/maths_input.rst:50
msgid "Cosine (Operator: cos() )"
msgstr "Co-seno (Operador: cos() )"

#: ../../reference_manual/maths_input.rst:53
msgid ""
"Gives you the tangent of the given angle. Usage: ``tan(50)`` Output: ``1.19``"
msgstr ""
"Devolve-lhe a tangente do ângulo indicado. Utilização: ``tan(50)`` "
"Resultado: ``1,19``"

#: ../../reference_manual/maths_input.rst:54
msgid "Tangent (Operator: tan() )"
msgstr "Tangente (Operador: tan() )"

#: ../../reference_manual/maths_input.rst:57
msgid ""
"Inverse function of the sine, gives you the angle which the sine equals the "
"argument. Usage: ``asin(0.76)`` Output: ``50``"
msgstr ""
"Função inversa do seno. Devolve-lhe o ângulo cujo seno é igual ao argumento. "
"Utilização: ``asin(0,76)`` Resultado: ``50``"

#: ../../reference_manual/maths_input.rst:58
msgid "Arc Sine (Operator: asin() )"
msgstr "Arco-Seno (Operador: asin() )"

#: ../../reference_manual/maths_input.rst:61
msgid ""
"Inverse function of the cosine, gives you the angle which the cosine equals "
"the argument. Usage: ``acos(0.64)`` Output: ``50``"
msgstr ""
"Função inversa do co-seno. Devolve-lhe o ângulo cujo co-seno é igual ao "
"argumento. Utilização: ``acos(0,64)`` Resultado: ``50``"

#: ../../reference_manual/maths_input.rst:62
msgid "Arc Cosine (Operator: acos() )"
msgstr "Arco-Co-Seno (Operador: acos() )"

#: ../../reference_manual/maths_input.rst:65
msgid ""
"Inverse function of the tangent, gives you the angle which the tangent "
"equals the argument. Usage: ``atan(1.19)`` Output: ``50``"
msgstr ""
"Função inversa da tangente. Devolve-lhe o ângulo cuja tangente é igual ao "
"argumento. Utilização: ``atan(1,19)`` Resultado: ``50``"

#: ../../reference_manual/maths_input.rst:66
msgid "Arc Tangent (Operator: atan() )"
msgstr "Arco-Tangente (Operador: atan() )"

#: ../../reference_manual/maths_input.rst:69
msgid ""
"Gives you the value without negatives. Usage: ``abs(75-100)`` Output: ``25``"
msgstr ""
"Devolve-lhe o valor sem negativos. Utilização: ``abs(75-100)`` Resultado: "
"``25``"

#: ../../reference_manual/maths_input.rst:70
msgid "Absolute (Operator: abs() )"
msgstr "Módulo (Operador: abs() )"

#: ../../reference_manual/maths_input.rst:73
msgid ""
"Gives you given values using e as the exponent. Usage: ``exp(1)`` Output: "
"``2.7183``"
msgstr ""
"Devolve-lhe valores que usam o 'e' com base do expoente. Utilização: "
"``exp(1)`` Resultado: ``2,7183``"

#: ../../reference_manual/maths_input.rst:74
msgid "Exponent (Operator: exp() )"
msgstr "Expoente (Operador: exp() )"

#: ../../reference_manual/maths_input.rst:77
msgid ""
"Gives you the natural logarithm, which means it has the inverse "
"functionality to exp(). Usage: ``ln(2)`` Output: ``0.6931``"
msgstr ""
"Devolve-lhe o logaritmo natural, o que significa que tem a funcionalidade "
"inversa do exp(). Utilização: ``ln(2)`` Resultado: ``0,6931``"

#: ../../reference_manual/maths_input.rst:79
msgid "Natural Logarithm (Operator: ln() )"
msgstr "Logaritmo Natural (Operador: ln() )"

#: ../../reference_manual/maths_input.rst:81
msgid "The following are technically supported but bugged:"
msgstr "As seguintes funções são tecnicamente suportados mas têm erros:"

#: ../../reference_manual/maths_input.rst:84
msgid ""
"Gives you logarithms of the given value. Usage: ``log10(50)`` Output: "
"``0.64``"
msgstr ""
"Devolve-lhe o logaritmo do valor indicado. Utilização: ``log10(50)`` "
"Resultado: ``0.64``"

#: ../../reference_manual/maths_input.rst:86
msgid "Common Logarithm (Operator: log10() )"
msgstr "Logaritmo Comum (Operador: log10() )"

#: ../../reference_manual/maths_input.rst:89
msgid "Order of Operations."
msgstr "Ordem das Operações."

#: ../../reference_manual/maths_input.rst:91
msgid ""
"The order of operations is a globally agreed upon reading order for "
"interpreting mathematical expressions. It solves how to read an expression "
"like:"
msgstr ""
"A ordem das operações é um acordo global sobre a ordem com que se lêem e "
"interpretam as expressões matemáticas. Ajuda na resolução de uma expressão "
"como a seguinte:"

#: ../../reference_manual/maths_input.rst:93
msgid "``2+3*4``"
msgstr "``2+3*4``"

#: ../../reference_manual/maths_input.rst:95
msgid ""
"You could read it as 2+3 = 5, and then 5*4 =20. Or you could say 3*4 = 12 "
"and then 2+12 = 14."
msgstr ""
"Podê-la-á ler como 2+3 = 5, e depois 5*4 =20. Mas também poderá ver que 3*4 "
"= 12 e depois 2+12 = 14."

#: ../../reference_manual/maths_input.rst:97
msgid ""
"The order of operations itself is Exponents, Multiplication, Addition, "
"Subtraction. So we first multiply, and then add, making the answer to the "
"above 14, and this is how Krita will interpret the above."
msgstr ""
"A ordem das operações em si é Expoentes, Multiplicação, Adição, Subtracção. "
"Como tal, primeiro multiplicamos, depois criamos a resposta com o valor 14 "
"acima, e é assim que o Krita irá interpretar o valor acima."

#: ../../reference_manual/maths_input.rst:99
msgid ""
"We can use brackets to specify certain operations go first, so to get 20 "
"from the above expression, we do the following:"
msgstr ""
"Podemos usar parêntesis para indicar que certas operações vão primeiro, para "
"saber como obter 20 da expressão acima que precisamos de fazer o seguinte:"

#: ../../reference_manual/maths_input.rst:101
msgid "``( 2+3 )*4``"
msgstr "``( 2+3 )*4``"

#: ../../reference_manual/maths_input.rst:103
msgid ""
"Krita can interpret the brackets accordingly and will give 20 from this."
msgstr ""
"O Krita consegue interpretar os parêntesis em conformidade e devolver-lhe-á "
"20 a partir do resultado."

#: ../../reference_manual/maths_input.rst:106
msgid "Errors"
msgstr "Erros"

#: ../../reference_manual/maths_input.rst:108
msgid ""
"Sometimes, you see the result becoming red. This means you made a mistake "
"and Krita cannot parse your maths expression. Simply click the input box and "
"try again."
msgstr ""
"Em alguns casos, poderá ver que o resultado vem a vermelho. Isto significa "
"que cometeu um erro e o Krita não consegue processar as suas expressões "
"matemáticas. Basta carregar no campo de texto e experimentar de novo."
