.. meta::
   :description:
        Describes the creation of SVG storyboard export templates.

.. metadata-placeholder

   :authors: - Emmet O'Neill <emmetoneill.pdx@gmail.com>
   :license: GNU free documentation license 1.3 or later.

===============================
SVG Storyboard Export Templates 
===============================

:program:`Krita`'s :ref:`storyboard docker` has a variety of options for exporting your storyboards to PDF or SVG file formats.

The simplest of those are the procedural layout options, such as "rows", "columns", and "grid" modes.
Using these modes, Krita can generate a somewhat basic page layout with just a few settings.
As a quick and easy preview or in a pinch, these modes do a decent job of showing you what you need to see.

However, for the highest level of control and the best aesthetics, the layout option that we recommend 
is one that makes use of an SVG template file. This template file, either written by hand or made in an SVG editor
program like :program:`Inkscape`, gives you full control over where and how your storyboard elements are to be placed on 
the final exported page, with full support for background and overlay layers.

Using an SVG template, you can hopefully export storyboards that fit the needs of whatever project you're working on,
or even fit the exact specifications of an existing storyboard paper format!

==============================================
Krita's Default SVG Storyboard Export Template
==============================================

For convenience, Krita ships with its own default SVG storyboard template that you're (of course) free to use for any project,
to modify to suit your needs, and to study from when creating your own SVG template files. (And, not to toot my own horn or anything
but I'd say it looks pretty cool, too!)

So, if you're simply looking for an awesome way to export and present the storyboards that you've made in Krita,
you can stop reading now and just use the SVG template file that comes with Krita's default resources. 

If you still want or need to create your own SVG storyboard export template file, however, read on!

============================
Vector Editing with Inkscape
============================

While it's perfectly possible to write one of these SVG template files by hand (if you're a masochist), the way that I recommend 
is to use a SVG editor like :program:`Inkscape`. Much like Krita, Inkscape is a free and open source program that you can use 
to create vector art, design logos, and more. In our case, we're going to use it to create a template file that Krita can understand
and work with. Inkscape is pretty good, so give it a try! https://inkscape.org/

This is not going to be a full Inkscape tutorial or anything (I couldn't give one if I wanted to, frankly), but this page should
hopefully give you the essential details needed to create a working storyboard export template.

=================================================
Designing Your Own SVG Storybaord Export Template
=================================================

*After opening Inkscape*, the first thing we need to do is select our preferred :guilabel:`Page Size` and :guilabel:`Display Unit`.
Both of these settings can be found in the "File > Document Properties..." menu inside of Inkscape.
I decided to go with :guilabel:`A4` paper and :guilabel:`mm` units.

- create 3 layers
- convert those layers into groups to rename them, then convert them back into layers.
- optionally add whatever design elements to the background and overlay layers that you want.
- add storyboard element layout rectangles to the layout layer, giving them appropriate labels.
- save your SVG file
- open the Krita document containing your storyboards
- and export as PDF or SVG
- feast your eyes on those beautiful results!
